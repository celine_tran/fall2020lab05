//Celine Tran 1938648
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {

	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,false);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> nonDuplicate = new ArrayList<String>();
		for (int i=0;i<input.size();i++) {

			String phrase=input.get(i);
			
			//if new array does not contains the phrase then add
			if(!(nonDuplicate.contains(phrase))) {
			nonDuplicate.add(phrase);
		}
	}
		return nonDuplicate;
	}
}
	


