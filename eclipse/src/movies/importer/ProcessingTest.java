//Celine Tran 1938648
package movies.importer;
import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		String source = "C:\\Users\\celine_tran\\courses\\java310\\fall2020lab05\\text_filesIn";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\fall2020lab05\\text_filesOut";
	
		LowercaseProcessor test1 = new LowercaseProcessor(source,destination);
		test1.execute();
		
		String source1 = "C:\\Users\\celine_tran\\courses\\java310\\fall2020lab05\\text_filesOut";
		String destination1 = "C:\\Users\\celine_tran\\courses\\java310\\fall2020lab05\\duplicateRemoved";
		
		RemoveDuplicates test2 = new RemoveDuplicates(source1,destination1);
		test2.execute();
		
	}

}
