//Celine Tran 1938648
package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {

	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,true);
	}


	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (int i=0;i<input.size();i++) {
			String lower= input.get(i).toLowerCase();
			System.out.println(lower);
			System.out.println(" ");
			asLower.add(lower);
		}
		
		return asLower;
	}
}
